## Scripts d'installation de VM à partir de qemu ou libvirt

### Libvirt installation sur Ubuntu :

```bash
sudo apt install qemu-kvm libvirt-daemon-system
sudo adduser $USER libvirt
sudo apt install virtinst
```
Documentation sur les commandes [ici](https://guide.ubuntu-fr.org/server/libvirt.html) et [là](https://ubuntu.com/server/docs/virtualization-libvirt)

### qemu installation sur Ubuntu :

```bash
sudo apt install qemu qemu-utils qemu-system-x86
```
