#!/bin/bash

if [ $# -ne 4 ]; then

  echo "Utilisation : $0 Renseigner les 4 champs : 'Nom de la VM' 'Quantité RAM' 'Nombre de CPU' 'Taille Disque'"

  exit 1
fi

NAME=$1
RAM_MB=$(($2 * 1024))
VCPUS=$3
DISK_GB=$4
G=G
PREDATE=$(date +%s)
DATE=$PREDATE

qemu-img create -f qcow2 -o preallocation=metadata ./$NAME$DATE.qcow2 $DISK_GB$G

sleep 1

qemu-system-x86_64 -smp 4 -m 4096 \
  -cdrom debian-live-11.1.0-amd64-standard.iso \
  -netdev user,id=n1 -device virtio-net-pci,netdev=n1 \
  -device virtio-scsi-pci -drive file=./$NAME$DATE.qcow2,if=none,id=drive-index-0,format=qcow2,cache=none -device scsi-hd,drive=drive-index-0
