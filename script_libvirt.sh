#!/bin/bash

if [ $# -ne 3 ]; then

  echo "Utilisation : $0 Renseigner les 3 champs : 'Nom de la VM' 'Quantité RAM' 'Nombre de CPU'"

  exit 1
fi

NAME=$1
RAM_MB=$(($2 * 1024))
VCPUS=$3
qemu-img create -f qcow2 ./$NAME.img 10G
sleep 1

virt-install \
  --virt-type kvm \
  --name $NAME \
  --ram $RAM_MB \
  --vcpus $VCPUS \
  --os-variant debian10 \
  --virt-type kvm \
  --hvm \
  --location ./debian-live-11.1.0-amd64-standard.iso \
  --network bridge=br0,model=virtio \
  --graphics none \
  --extra-args "console=tty0 console=ttyS0,115200n8" \
  --disk path=./$NAME.img,size=10
